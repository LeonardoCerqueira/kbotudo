import discord
import asyncio 
import random
from discord.ext import commands
from discord.ext.commands import Bot

TOKEN = 'NjkwNTc4MzU1MjU0MTk4MzUy.XnTdlQ.c_dhaEX6qpMaAQPeT5aNS6pZVpk'

client = Bot("!")

@client.command(name="allstar", 
                description="Prints the allstar lyrics",
                brief="Prints the allstar lyrics with text-to-speech")
async def allstar_lyrics(context):
    lyrics1 = """Somebody once told me the world is gonna roll me
I ain't the sharpest tool in the shed
She was looking kind of dumb with her finger and her thumb
In the shape of an "L" on her forehead
Well the years start coming and they don't stop coming
Fed to the rules and I hit the ground running
Didn't make sense not to live for fun
Your brain gets smart but your head gets dumb
So much to do, so much to see
So what's wrong with taking the back streets?
You'll never know if you don't go
You'll never shine if you don't glow
Hey now, you're an all-star, get your game on, go play
Hey now, you're a rock star, get the show on, get paid
And all that glitters is gold
Only shooting stars break the mold
It's a cool place and they say it gets colder"""

    lyrics2 = """You're bundled up now, wait till you get older
But the meteor men beg to differ
Judging by the hole in the satellite picture
The ice we skate is getting pretty thin
The water's getting warm so you might as well swim
My world's on fire, how about yours?
That's the way I like it and I never get bored
Hey now, you're an all-star, get your game on, go play
Hey now, you're a rock star, get the show on, get paid
All that glitters is gold
Only shooting stars break the mold
Hey now, you're an all-star, get your game on, go play
Hey now, you're a rock star, get the show, on get paid
And all that glitters is gold
Only shooting stars
Somebody once asked could I spare some change for gas?"""

    lyrics3 = """I need to get myself away from this place
I said yep what a concept
I could use a little fuel myself
And we could all use a little change
Well, the years start coming and they don't stop coming
Fed to the rules and I hit the ground running
Didn't make sense not to live for fun
Your brain gets smart but your head gets dumb
So much to do, so much to see
So what's wrong with taking the back streets?
You'll never know if you don't go (go!)
You'll never shine if you don't glow
Hey now, you're an all-star, get your game on, go play
Hey now, you're a rock star, get the show on, get paid
And all that glitters is gold
Only shooting stars break the mold
And all that glitters is gold
Only shooting stars break the mold"""

    await context.send(lyrics1)
    await context.send(lyrics2)
    await context.send(lyrics3)
    await context.send("https://media.giphy.com/media/8dxRUHVe7UULe/giphy.gif")

@client.command(name="oi", 
                description="Greets the user with a 'Olar'",
                brief="Greets the user",
                aliases=["olá", "ola", "olar"],
                pass_context=True)
async def greet(context):
    await context.send("Olar " + context.message.author.mention, tts=True)

@client.command(name="tchau", 
                description="Says bye to the user",
                brief="Says bye to the user")
async def bye(context, arg1):
    await context.send("Sayunara " + arg1, tts=True)

@client.command(name="8ball", 
                description="Answers from the beyond",
                brief="Answers a yes or no question")
async def eight_ball(context):
    possible_responses = [
        "Nem aqui nem na China",
        "Bem capaz que não",
        "Não faço a mínima ideia",
        "É possivelmente possível",
        "Com certeza",
    ]

    if "curios" in context.message.content.lower():
        response = "Hmmm, bababea..."
    else:
        response = random.choice(possible_responses) + ", "  + context.message.author.mention

    await context.send(response, tts=True)

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run(TOKEN)